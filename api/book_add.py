import sys
from urllib import request
import json
import feedparser
import time

def ndl_search(isbn):
    time.sleep(15)
    base_url = 'https://iss.ndl.go.jp/api/opensearch?dpid=iss-ndl-opac&isbn='
    target_url = base_url + isbn

    d = feedparser.parse(target_url)

    rel_url = d['entries'][0]['link']
    json_url = rel_url + '.json'

    with request.urlopen(json_url) as res:
        data = json.load(res)
        title = data['title'][0]['value']
        title_yomi = data['title'][0]['transcription']
        creator = data['creator'][0]['name']
        creator_yomi = data['creator'][0]['transcription']
        dc_creator = data['dc_creator'][0]['name']
        isbn_t = data['identifier']['ISBN'][0]
        if 'volume' in data:
            volume = data['volume'][0]
            dict_data = {
                'title': title,
                'title_yomi': title_yomi,
                'volume': volume,
                'creator': creator,
                'creator_yomi': creator_yomi,
                'author': dc_creator,
                'isbn': isbn_t
            }
        else:
            dict_data = {
                'title': title,
                'title_yomi': title_yomi,
                'volume': 0,
                'creator': creator,
                'creator_yomi': creator_yomi,
                'author': dc_creator,
                'isbn': isbn_t
            }
        json_data = json.dumps(dict_data, ensure_ascii=False)
        return json_data

if __name__ == '__main__':
    ndl_search(sys.argv[1])