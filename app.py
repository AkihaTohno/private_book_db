from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify
from flask_mongoengine import MongoEngine
from api.book_add import ndl_search
import json

import ast
app = Flask(__name__)
app.config['MONGODB_SETTINGS'] = {
    'db': 'db',
    'host': 'localhost',
    'port': 27017
}
db = MongoEngine()
db.init_app(app)

class bookinfo(db.Document):
    title = db.StringField()
    title_yomi = db.StringField()
    volume = db.StringField()
    creator = db.StringField()
    creator_yomi = db.StringField()
    author = db.StringField()
    isbn = db.StringField()


@app.route('/', methods=['GET'])
def index():
    booklist = bookinfo.objects.all()
    return render_template('index.html', booklist=booklist)


@app.route('/', methods=['POST'])
def index2():
    json_data = request.json
    return render_template('list.html', booklist=json_data)

@app.route('/add', methods=['POST'])
def add_book():
    isbn = request.form['isbn']
    try:
        rel = ndl_search(isbn)
    except Exception as e:

        return json.dumps({'status': 'Failed', 'MSG': e})
    else:
        try:
            rel = ast.literal_eval(rel)
            bookdata = bookinfo(isbn=rel['isbn'])
            bookdata.title = rel['title']
            bookdata.title_yomi = rel['title_yomi']
            bookdata.author = rel['author']
            bookdata.creator = rel['creator']
            bookdata.creator_yomi = rel['creator_yomi']
            bookdata.volume = str(rel['volume'])
            bookdata.save()
            new_booklist = bookinfo.objects.all()
        except Exception as e:
            return jsonify({"status": "Failed", "MSG": e})
        else:
            return jsonify({"status": "Success", "booklist": new_booklist})


if __name__ == '__main__':
    app.run()
